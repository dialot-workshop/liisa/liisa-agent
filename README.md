# Liisa Agent

Agent use to report if the host is declared healthy or not.

## Getting started

In order to use this agent, you can create files under `BASE_AGENT_CONFIG_FOLDER` depending on the implementation that you want to support.

### Http Health Check

If you want your agent to validate an endpoint, you can create the file `http-get-health` that contains a list of accessible endpoint on each new lines.

### Servicectl Health Check

If you want you agent to validate that a background service is running, you can create the file `servicectl-health` and list all services that should be looked at on each new lines.

## Local Development

### Initial Setup

Start by importing the code

```bash
foo@bar:~/liisa-agent/$ git https://gitlab.com/dialot-workshop/liisa/liisa-agent.git
foo@bar:~/liisa-agent/$ git checkout my-new-branch
```

Then you can validate that the project is running properly by running this command:

```bash
foo@bar:~/liisa-agent/$ go run ./cmd/liisa-agent/
```

The agent should now run on port 8080, and you should be able to call the endpoint with, per example, a simple curl command.

```bash
foo@bar:~/liisa-agent/$ curl -i localhost:8080/health
```

### Adding a new implementation

One of the goal of this health check, is to be able to add easily new implementation to make it generic and easily configurable. The idea would be to have the possibility to configure different liisa-agent with different health check without having to modify the code.

For adding a new implementation, the interface `HealthCheckValidator.go` exist and should be implemented. Then, in the method `statusHandler` in the file `liisa-agent.go` you new object should be added to the list of supported implementation.

## Collaboration

- In order to collaborate on this project, you can either create a new branch, and then open a merge request to the `master`. Or you can Fork the project, do you modification, and ask for a Pull Request from you updated Fork if needed.

## Test

The unit test are being run on each pipeline execution on master and on each merge request.

They can be un locally with the following commands

```bash
foo@bar:~/liisa-agent/$ go test ./...
```

## Deploy

This agent should run on startup of the instance. The packaging as yet to come ... 