package healthcheck


// Interface for an health check validator
//
// The implementation of an health check validator should 
// validate at 1 type of service/issue and validate if it should be
// considered healthy or not. 
type HealthCheckValidator interface {

	// Should return true or false if it should be considered healthy or not.
	ValidateCheck() bool

	// Returned the status of that specific health check as a string.
	// Can contains information on specific part of the validator that is failing,
	// or general information that can be useful to find the root of the problem.
	GetHealthStatus() string

	// Returned true or false depending if this health check validator should be use or not
	// based on the current configuration of the system.
	IsEnabled() bool
}