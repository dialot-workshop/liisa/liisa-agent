package healthcheck


// Dummy HealthCheckValidator implementation.
//
// This can be use to validate the main liisa-agent logic, and also return a 
// positive status if there's no validator enabled.
type DummyHealthCheckValidator struct {}

func (dummy DummyHealthCheckValidator) ValidateCheck() bool {
	return true
}

func (dummy DummyHealthCheckValidator) GetHealthStatus() string {
	return "dummyHealthCheck : Healthy"
}

func (dummy DummyHealthCheckValidator) IsEnabled() bool {
	return false
}