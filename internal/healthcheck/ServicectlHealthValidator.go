package healthcheck

import (
	"os/exec"
	"bufio"
	"fmt"
	"os"
)

// Constructor for a health check that will valide
// if some specific systemctl service is running properly
type ServicectlHealthCheckValidator struct {}

func (validator ServicectlHealthCheckValidator) ValidateCheck() bool {
	
	// Get list of systemctl services that we should validate
	configFile := BASE_AGENT_CONFIG_FOLDER + "servicectl-health"

	lines, err := readLines(configFile)
    if err != nil {
        fmt.Printf("readLines: %s", err)
		os.Exit(1)
    }

	// loop and execute systemctl check for each service
	var isHealthy bool = true
    for _, line := range lines {
		fmt.Print("line :", line)
		if !executeSystemctlCheck(line) {
			isHealthy = false
		}
    }

	return isHealthy
}

func (validator ServicectlHealthCheckValidator) GetHealthStatus() string {
	// todo add implementation
	return "Servicectl: Healthy"
}

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
    file, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    var lines []string
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines, scanner.Err()
}

// Execute a systemctl check for a given service.
//
// Will return true of false based if the service is running or not and will exit with an 
// error if it is not even able to execute the systemctl command.
func executeSystemctlCheck(systemctl string) bool {
	cmd := exec.Command("systemctl", "check", systemctl)
	out, err := cmd.CombinedOutput()
	if err != nil {
	  if exitErr, ok := err.(*exec.ExitError); ok {
		fmt.Printf("systemctl finished with non-zero: %v\n", exitErr)
	  } else {
		fmt.Printf("failed to run systemctl: %v", err)
		os.Exit(1)
	  }
	}
	fmt.Printf("Status is: %s\n", string(out))
	return true
}

// This healthCheck is enable if a config file exist on the system.
func (validator ServicectlHealthCheckValidator) IsEnabled() bool {

	// Get list of systemctl services that we should validate
	configFile := BASE_AGENT_CONFIG_FOLDER + "servicectl-health"

	_, err := os.Stat(configFile)
	return err == nil
}