package healthcheck

import (
	"net/http"
	"log"
	"fmt"
	"os"
 )

// Implementation of the HealthCheckValidator for Http endpoint
//
// This validator will look at given endpoint and execute a get request to know if it is 
// healthy are not (based on the response code).
type HttpGetHealthCheckValidator struct {}


func (validator HttpGetHealthCheckValidator) ValidateCheck() bool {
    
	// Get list of systemctl services that we should validate
	configFile := BASE_AGENT_CONFIG_FOLDER + "http-get-health"

	lines, err := readLines(configFile)
    if err != nil {
        fmt.Printf("readLines: %s", err)
		os.Exit(1)
    }

	// loop and execute systemctl check for each service
	var isHealthy bool = true
    for _, line := range lines {
		if !executeHttpGetCheck(line) {
			isHealthy = false
		}
    }

	return isHealthy
}

// Call a given endpoint and return true if the response code is between 200 and 299.
func executeHttpGetCheck(uri string) bool {
	resp, err := http.Get(uri)
    if err != nil {
        log.Fatal(err)
    }

    return resp.StatusCode >= 200 && resp.StatusCode <= 299
}

func (validator HttpGetHealthCheckValidator) GetHealthStatus() string {
	// todo add implementation
	return "HttpGetHealthCheck : Healthy"
}

// This healthCheck is enable if a config file exist on the system.
func (validator HttpGetHealthCheckValidator) IsEnabled() bool {

	// Get list of systemctl services that we should validate
	configFile := BASE_AGENT_CONFIG_FOLDER + "http-get-health"

	_, err := os.Stat(configFile)
	return err == nil
}