package healthcheck

import (
	"testing"
)

func TestDummyValidateCheck(t *testing.T) {

	dummyHealthCheck := new(DummyHealthCheckValidator)
	got := dummyHealthCheck.ValidateCheck()
	want := true
	
	if got != want {
		t.Errorf("got %t, wanted %t", got, want)
	}

}

func TestDummyHealthStatus(t *testing.T) {

	dummyHealthCheck := new(DummyHealthCheckValidator)
	got := dummyHealthCheck.GetHealthStatus()
	want := "dummyHealthCheck : Healthy"
	
	if got != want {
		t.Errorf("got %s, wanted %s", got, want)
	}

}

func TestDummyIsEnabled(t *testing.T) {

	dummyHealthCheck := new(DummyHealthCheckValidator)
	got := dummyHealthCheck.IsEnabled()
	want := false
	
	if got != want {
		t.Errorf("got %t, wanted %t", got, want)
	}

}