package main

import (
	hc "liisa-agent/internal/healthcheck"

	"encoding/json"
	"net/http"
	"fmt"
	"log"
)

// Execute all Health Check Validators, and contains the main logic to know what type of
// message/code we should return to the caller.
//
// It returns a function that can be given to a function handle (like http.HandleFunc) for a running server.
func statusHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
 
		// Add new validator implementation in here
		allValidators := []hc.HealthCheckValidator {
			new(hc.DummyHealthCheckValidator),
			new(hc.ServicectlHealthCheckValidator), 
			new(hc.HttpGetHealthCheckValidator),
		}

		// Only get the one that are enable/valid
		enabledValidators := getEnabledValidators(allValidators)

		// get the global health and all returned status
		isHealthy, returnedStatus := runHealthCheck(enabledValidators)

		// if one validator fails, return 500 code
		bytes, err := json.MarshalIndent(returnedStatus, "", "\t")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else if !isHealthy {
			http.Error(w, "", http.StatusInternalServerError)
		}
		w.Write(bytes)
	}
}

// Receive a list of HealthCheckValidator, and returned only the ones that are enable.
func getEnabledValidators(allValidators []hc.HealthCheckValidator) []hc.HealthCheckValidator {
	enabledValidators := []hc.HealthCheckValidator{}
	for _, validator := range allValidators {
		if validator.IsEnabled() {
			enabledValidators = append(enabledValidators, validator)
		}
	}
	return enabledValidators
}

// Validate the HealthCheck status of each given implementation of HealthCheckValidator.
//
// Will return true or false depending if there's one health check that is failing. And will
// return all the compile status  of each validators.
func runHealthCheck(enabledValidators []hc.HealthCheckValidator) (bool, []string) {
	var valid bool = true
	returnedStatus := []string{}
	for _, validator := range enabledValidators {
		returnedStatus = append(returnedStatus, validator.GetHealthStatus())
		if !validator.ValidateCheck() {
			valid = false
		}
	}
	return valid, returnedStatus
}

// Main function of the agent. 
//
// This will start a server on port 8080 that can be access to know the status of different components.
func main() {
    fmt.Println("Starting Liisa-agent ...")
	http.HandleFunc("/health", statusHandler())
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

